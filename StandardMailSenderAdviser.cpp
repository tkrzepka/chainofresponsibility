//
// Created by Tomasz Rzepka on 6/05/15.
//

#include <iostream>
#include "StandardMailSenderAdviser.h"

using namespace std;

void StandardMailSenderAdviser::adviseSendingWay(const Cargo &p_cargo) const {
  if(p_cargo.weight<0.5){
    switch (p_cargo.destination){
      case Cargo::Poland:
      case Cargo::Germany:
      case Cargo::France:
      case Cargo::Slovakia:
      case Cargo::Austria:
      case Cargo::Switzerland:
      case Cargo::Monaco:
        cout<<"Package: "<<p_cargo.package_name<<". Should be sent by standard mail."<<endl;
            break;
      case Cargo::Australia:
      case Cargo::Japan:
      case Cargo::China:
      case Cargo::Singapore:
      case Cargo::Colombia:
      case Cargo::Mexico:
      case Cargo::Brazil:
      case Cargo::SouthCorea:
      case Cargo::NorthCorea:
        CargoSenderAdviser::adviseSendingWay(p_cargo);
            break;
    }

  }
  else
    CargoSenderAdviser::adviseSendingWay(p_cargo);
}

void StandardMailSenderAdviser::addAdviser(std::unique_ptr<ICargoSenderAdviser> &&p_adviser) {
  CargoSenderAdviser::addAdviser(move(p_adviser));
}

StandardMailSenderAdviser::~StandardMailSenderAdviser() {

}
