//
// Created by Tomasz Rzepka on 6/05/15.
//

#include <iostream>
#include "CourierMailSenderAdviser.h"

using namespace std;

void CourierMailSenderAdviser::adviseSendingWay(const Cargo &p_cargo) const {
    if(p_cargo.weight<20){
        switch (p_cargo.destination){
            case Cargo::Poland:
            case Cargo::Germany:
            case Cargo::France:
            case Cargo::Slovakia:
            case Cargo::Austria:
            case Cargo::Switzerland:
            case Cargo::Monaco:
                cout<<"Package: "<<p_cargo.package_name<<". Should be sent by courier."<<endl;
                break;
            case Cargo::Australia:
            case Cargo::Japan:
            case Cargo::China:
            case Cargo::Singapore:
            case Cargo::Colombia:
            case Cargo::Mexico:
            case Cargo::Brazil:
            case Cargo::SouthCorea:
            case Cargo::NorthCorea:
                CargoSenderAdviser::adviseSendingWay(p_cargo);
                break;
        }
    }
    else
        CargoSenderAdviser::adviseSendingWay(p_cargo);
}

void CourierMailSenderAdviser::addAdviser(std::unique_ptr<ICargoSenderAdviser> &&p_adviser) {
    CargoSenderAdviser::addAdviser(move(p_adviser));
}

CourierMailSenderAdviser::~CourierMailSenderAdviser() {

}
