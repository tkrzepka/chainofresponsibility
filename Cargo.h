//
// Created by Tomasz Rzepka on 6/05/15.
//

#pragma once

#include <string>

struct Cargo{
    enum Country{
        Poland,
        Germany,
        France,
        Slovakia,
        Austria,
        Switzerland,
        Monaco,
        Australia,
        Japan,
        China,
        Singapore,
        Colombia,
        Mexico,
        Brazil,
        SouthCorea,
        NorthCorea,
    };

    Cargo(const std::string &package_name, const Country &destination, float weight) : package_name(package_name),
                                                                                       destination(destination),
                                                                                       weight(weight) { }

    std::string package_name;
    Country destination;
    float weight;
};

