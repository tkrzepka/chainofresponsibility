//
// Created by Tomasz Rzepka on 6/05/15.
//

#include <iostream>
#include "AircraftTransportSenderAdviser.h"

using namespace std;

void AircraftTransportSenderAdviser::adviseSendingWay(const Cargo &p_cargo) const {
    if(p_cargo.weight<=100){
        switch (p_cargo.destination){


            case Cargo::Australia:
            case Cargo::Japan:
            case Cargo::China:
            case Cargo::Singapore:
            case Cargo::Colombia:
            case Cargo::Mexico:
            case Cargo::Brazil:
            case Cargo::SouthCorea:
            case Cargo::NorthCorea:
                cout<<"Package: "<<p_cargo.package_name<<". Should be sent by aircraft."<<endl;
                break;
            case Cargo::Poland:
            case Cargo::Germany:
            case Cargo::France:
            case Cargo::Slovakia:
            case Cargo::Austria:
            case Cargo::Switzerland:
            case Cargo::Monaco:
                CargoSenderAdviser::adviseSendingWay(p_cargo);
                break;
        }

    }
    else
        CargoSenderAdviser::adviseSendingWay(p_cargo);
}

void AircraftTransportSenderAdviser::addAdviser(std::unique_ptr<ICargoSenderAdviser> &&p_adviser) {
    CargoSenderAdviser::addAdviser(move(p_adviser));
}

AircraftTransportSenderAdviser::~AircraftTransportSenderAdviser() {

}
