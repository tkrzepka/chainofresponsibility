//
// Created by Tomasz Rzepka on 6/05/15.
//

#include <iostream>
#include "CargoSenderAdviser.h"

using namespace std;

void CargoSenderAdviser::adviseSendingWay(const Cargo& p_cargo) const {
    if(m_nextAdviser != nullptr)
        m_nextAdviser->adviseSendingWay(p_cargo);
}

void CargoSenderAdviser::addAdviser(std::unique_ptr<ICargoSenderAdviser>&& p_adviser) {
    ICargoSenderAdviser * l_adviser;
    l_adviser = this;
    while (l_adviser->m_nextAdviser != nullptr){
        l_adviser = l_adviser->m_nextAdviser.get();
    }
    l_adviser->m_nextAdviser = move(p_adviser);
}