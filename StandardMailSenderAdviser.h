//
// Created by Tomasz Rzepka on 6/05/15.
//

#pragma once

#include "CargoSenderAdviser.h"

class StandardMailSenderAdviser: public CargoSenderAdviser {

public:
    virtual void adviseSendingWay(const Cargo &p_cargo) const override;

    virtual void addAdviser(std::unique_ptr<ICargoSenderAdviser> &&p_adviser) override;

    virtual ~StandardMailSenderAdviser();
};
