//
// Created by Tomasz Rzepka on 6/05/15.
//

#include <iostream>
#include "DefaultSenderAdviser.h"

using namespace std;

void DefaultSenderAdviser::adviseSendingWay(const Cargo& p_cargo) const {
    cout<<"Couldn't deduce sending strategy for: "<<p_cargo.package_name<<". sorry"<<endl;
}

void DefaultSenderAdviser::addAdviser(unique_ptr<ICargoSenderAdviser> &&p_adviser) {
    CargoSenderAdviser::addAdviser(move(p_adviser));
}