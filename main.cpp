#include <memory>
#include <iostream>
#include <deque>
#include "AircraftTransportSenderAdviser.h"
#include "CourierMailSenderAdviser.h"
#include "FreightTransportSenderAdviser.h"
#include "PrivateTransportSenderAdviser.h"
#include "StandardMailSenderAdviser.h"
#include "CargoSenderAdviser.h"
#include "DefaultSenderAdviser.h"

using namespace std;

int main() {
    cout << "Hello, World!" << endl;
    auto cargoAdviser = make_unique<StandardMailSenderAdviser>();
    cargoAdviser->addAdviser(make_unique<CourierMailSenderAdviser>());
    cargoAdviser->addAdviser(make_unique<PrivateTransportSenderAdviser>());
    cargoAdviser->addAdviser(make_unique<AircraftTransportSenderAdviser>());
    cargoAdviser->addAdviser(make_unique<FreightTransportSenderAdviser>());
    cargoAdviser->addAdviser(make_unique<DefaultSenderAdviser>());
    deque<Cargo> cargoHolder;
    cargoHolder.emplace_back("test",Cargo::Country::Poland,100);
    cargoHolder.emplace_back("test2",Cargo::Country::Australia,80);
    cargoHolder.emplace_back("test3",Cargo::Country::Australia,101);
    cargoHolder.emplace_back("test4",Cargo::Country::Germany,18);
    cargoHolder.emplace_back("test5",Cargo::Country::Monaco,0.2);
    cargoHolder.emplace_back("test6",Cargo::Country::NorthCorea,0.2);
    for(const auto& i: cargoHolder)
        cargoAdviser->adviseSendingWay(i);
    return 0;
}