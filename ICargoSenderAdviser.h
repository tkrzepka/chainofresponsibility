//
// Created by Tomasz Rzepka on 6/05/15.
//

#pragma once

#include "Cargo.h"
#include "CargoSenderAdviser.h"
#include <memory>

class ICargoSenderAdviser {
public:
    virtual void adviseSendingWay(const Cargo& p_cargo) const = 0;
    virtual void addAdviser(std::unique_ptr<ICargoSenderAdviser>&& p_adviser) = 0;
    virtual ~ICargoSenderAdviser() {}

private:
    std::unique_ptr<ICargoSenderAdviser> m_nextAdviser {nullptr};
    friend class CargoSenderAdviser;
};
